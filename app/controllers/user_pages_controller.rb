class UserPagesController < ApplicationController
  def regist
    @user = User.new
  end

  def profile
  end

  def login
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if params[:back]
      render 'regist'
    elsif @user.save
      @user.send_activation_email
      flash[:info] = "入力されたメールアドレスにメールを送信しました。"
      redirect_to root_url
    else
      render 'regist'
    end
  end

  def confirm
    @user = User.new(user_params)
    render 'regist' if @user.invalid?
  end

  private

  def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
  end
end
