Rails.application.routes.draw do
  get 'menu' => 'menu_pages#menu'

  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  get 'signup' => 'user_pages#regist'

  get 'user/profile' =>'user_pages#profile'

  post 'signup/confirm' => 'user_pages#confirm'

  #get 'login' =>'user_pages#login'

  get 'about' =>'static_pages/about'

  root 'static_pages#home'

  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  resources :user_pages
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
end
